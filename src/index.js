const express = require("express");
const formdata = require("express-form-data");
const bbb = require("bigbluebutton-js");

const {
  PORT = 3000,
  BBB_URL = "",
  BBB_SECRET = "",
  ROOM_CONFIG = "",
} = process.env;

let roomConfig = {};
if (ROOM_CONFIG.length) {
  roomConfig = JSON.parse(ROOM_CONFIG);
}

if (!Object.keys(roomConfig).length) {
  console.log(
    "Invalid Room Configuration, Please set ROOM_CONFIG environment variable"
  );
}

const app = express();
const bbbApi = bbb.api(BBB_URL, BBB_SECRET);

app.use(formdata.parse());
app.use(formdata.format());
app.use(formdata.stream());
app.use(formdata.union());

app.get("/", (req, res) => {
  res.redirect("https://linuxappsummit.org/");
});

app.post("/:roomAlias", (req, res) => {
  const { roomAlias = "" } = req.params;

  if (Object.keys(roomConfig).indexOf(roomAlias) < 0) {
    res.status(404).send("Room does not exist");
    return;
  }

  const {
    roomId,
    attendeePassword,
    moderatorPassword,
    attendeePins,
    moderatorPins,
  } = roomConfig[roomAlias];

  const { attendeepin, moderatorpin, name = "Attendee", debug } = req.body;

  if (moderatorPins.length && moderatorpin) {
    if (moderatorPins.indexOf(moderatorpin) < 0) {
      res.status(403).send("Invalid Pin");
      return;
    }

    const meetingJoinUrl = bbbApi.administration.join(
      name,
      roomId,
      moderatorPassword,
      {
        joinViaHtml5: true,
      }
    );

    if (debug) {
      res.send(meetingJoinUrl);
      return;
    }

    res.redirect(meetingJoinUrl);
    return;
  }

  if (attendeePins.length) {
    if (!attendeepin) {
      res.status(403).send("Invalid Pin");
      return;
    }

    if (attendeePins.indexOf(attendeepin) < 0) {
      res.status(403).send("Invalid Pin");
      return;
    }

    const meetingJoinUrl = bbbApi.administration.join(
      name,
      roomId,
      attendeePassword,
      {
        joinViaHtml5: true,
      }
    );

    if (debug) {
      res.send(meetingJoinUrl);
      return;
    }

    res.redirect(meetingJoinUrl);
  }
});

app.listen(PORT, () => {
  console.log(`Listening on :: http://0.0.0.0:${PORT}`);
});
